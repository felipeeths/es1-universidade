package com.es1.classe;

public class Disciplina {
	String NomeDisciplina;
	String Optativa;
	int AnoLetivo;
	int Semestre;
	int IdCurso;
	int IdDisciplina;
	public String getNomeDisciplina() {
		return NomeDisciplina;
	}
	public void setNomeDisciplina(String nomeDisciplina) {
		NomeDisciplina = nomeDisciplina;
	}
	public String getOptativa() {
		return Optativa;
	}
	public int getAnoLetivo() {
		return AnoLetivo;
	}
	public void setAnoLetivo(int anoLetivo) {
		AnoLetivo = anoLetivo;
	}
	public int getSemestre() {
		return Semestre;
	}
	public void setSemestre(int semestre) {
		Semestre = semestre;
	}
	public void setOptativa(String optativa) {
		Optativa = optativa;
	}
	public int getIdDisciplina() {
		return IdDisciplina;
	}
	public void setIdDisciplina(int idDisciplina) {
		IdDisciplina = idDisciplina;
	}
	public int getIdCurso() {
		return IdCurso;
	}
	public void setIdCurso(int i) {
		IdCurso = i;
	}
}

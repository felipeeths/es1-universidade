package com.es1.classe;

public class Professor {
	String Nome;
	String Email;
	int Telefone;
	int CPF;
	int RG;
	int IdProfessor;
	public int getIdProfessor() {
		return IdProfessor;
	}
	public void setIdProfessor(int idProfessor) {
		IdProfessor = idProfessor;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public int getTelefone() {
		return Telefone;
	}
	public void setTelefone(int telefone) {
		Telefone = telefone;
	}
	public int getCPF() {
		return CPF;
	}
	public void setCPF(int cPF) {
		CPF = cPF;
	}
	public int getRG() {
		return RG;
	}
	public void setRG(int rG) {
		RG = rG;
	}
}

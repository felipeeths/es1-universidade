package com.es1.classe;

public class MatriculaADisciplina {
	int idAluno;
	int idDisciplina;
	int idAlunoDisciplina;
	public int getIdAluno() {
		return idAluno;
	}
	public void setIdAluno(int idAluno) {
		this.idAluno = idAluno;
	}
	public int getIdDisciplina() {
		return idDisciplina;
	}
	public void setIdDisciplina(int idDisciplina) {
		this.idDisciplina = idDisciplina;
	}
	public int getIdAlunoDisciplina() {
		return idAlunoDisciplina;
	}
	public void setIdAlunoDisciplina(int idAlunoDisciplina) {
		this.idAlunoDisciplina = idAlunoDisciplina;
	}
	
}

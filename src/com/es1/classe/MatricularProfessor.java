package com.es1.classe;

public class MatricularProfessor {
	int idProfessor;
	int idDisciplina;
	String DataInicio;
	public String getDataInicio() {
		return DataInicio;
	}
	public void setDataInicio(String data) {
		DataInicio = data;
	}
	public int getIdProfessor() {
		return idProfessor;
	}
	public void setIdProfessor(int idProfessor) {
		this.idProfessor = idProfessor;
	}
	public int getIdDisciplina() {
		return idDisciplina;
	}
	public void setIdDisciplina(int idDisciplina) {
		this.idDisciplina = idDisciplina;
	}
	
}

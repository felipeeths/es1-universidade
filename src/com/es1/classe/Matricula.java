package com.es1.classe;

public class Matricula {
	int IdAluno;
	int IdMatricula;
	int IdCurso;
	String Data;
	public int getIdMatricula() {
		return IdMatricula;
	}
	public void setIdMatricula(int idMatricula) {
		IdMatricula = idMatricula;
	}
	
	public int getIdAluno() {
		return IdAluno;
	}
	public void setIdAluno(int idAluno) {
		IdAluno = idAluno;
	}
	public int getIdCurso() {
		return IdCurso;
	}
	public void setIdCurso(int idCurso) {
		IdCurso = idCurso;
	}
	public String getData() {
		return Data;
	}
	public void setData(String data) {
		Data = data;
	}
}

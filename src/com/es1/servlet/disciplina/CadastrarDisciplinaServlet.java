package com.es1.servlet.disciplina;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Curso;
import com.es1.classe.Disciplina;
import com.es1.dao.BDCurso;
import com.es1.dao.BDDisciplinas;
import com.es1.service.DisciplinaService;

/**
 * Servlet implementation class CadastrarDisciplinaServlet
 */
@WebServlet("/CadastrarDisciplinaServlet")
public class CadastrarDisciplinaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CadastrarDisciplinaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DisciplinaService disciplinaservice = new DisciplinaService();
		String Nome = request.getParameter("Nome");
		String Optativa = request.getParameter("Optativa");
		int Semestre = Integer.parseInt(request.getParameter("Semestre"));
		int AnoLetivo = Integer.parseInt(request.getParameter("AnoLetivo"));
		String IdCurso = request.getParameter("Curso");
		Boolean Achou = disciplinaservice.CadastrarDisciplina(Nome, Optativa, Semestre,AnoLetivo, IdCurso);
		if(Achou == true){
			request.setAttribute("Achou","true");
			request.getRequestDispatcher("CadastroDisciplina.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("CadastroDisciplina.jsp").forward(request,response);
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

package com.es1.servlet.disciplina;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Disciplina;
import com.es1.dao.BDDisciplinas;
import com.es1.service.DisciplinaService;

/**
 * Servlet implementation class ExcluirDisciplinaServlet
 */
@WebServlet("/ExcluirDisciplinaServlet")
public class ExcluirDisciplinaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExcluirDisciplinaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DisciplinaService disciplinaservice = new DisciplinaService();
		String NomeExcluir = request.getParameter("NomeExcluir");
		Boolean Achou = disciplinaservice.ExcluirDisciplina(NomeExcluir);
		if (Achou == true) {	
	
			request.setAttribute("Achou","true");
			request.getRequestDispatcher("ExcluirDisciplina.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("ExcluirDisciplina.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

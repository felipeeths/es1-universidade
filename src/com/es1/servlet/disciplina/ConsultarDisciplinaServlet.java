package com.es1.servlet.disciplina;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Curso;
import com.es1.classe.Disciplina;
import com.es1.dao.BDCurso;
import com.es1.dao.BDDisciplinas;

/**
 * Servlet implementation class ConsultarDisciplinaServlet
 */
@WebServlet("/ConsultarDisciplinaServlet")
public class ConsultarDisciplinaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarDisciplinaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Disciplina disciplina = new Disciplina();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		String Nome = request.getParameter("Nome");
		Boolean Achou = bddisciplina.Search(disciplina, Nome);
		String Optativa = disciplina.getOptativa();
		int Semestre = disciplina.getSemestre();
		int AnoLetivo = disciplina.getAnoLetivo();
		String IdCurso = String.valueOf(disciplina.getIdCurso());
		Boolean AchouId = bdcurso.Search2(curso,IdCurso);
		String Curso = curso.getNomeCurso();
		if (Achou == true) {
			request.setAttribute("Nome",Nome);
			request.setAttribute("Optativa",Optativa);
			request.setAttribute("Semestre",Semestre);
			request.setAttribute("AnoLetivo",AnoLetivo);
			request.setAttribute("Curso",Curso);;
			request.getRequestDispatcher("TelaConsultarDisciplina.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("ConsultarDisciplina.jsp").forward(request,response);
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

package com.es1.servlet.matricula;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.service.MatricularService;

/**
 * Servlet implementation class MatricularProfessor
 */
@WebServlet("/MatricularProfessor")
public class MatricularProfessorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MatricularProfessorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MatricularService matricularservice = new MatricularService();
		String Nome = request.getParameter("Nome");
		String Disciplina = request.getParameter("Disciplina");
		Boolean Validade = matricularservice.MatricularProfessor(Nome,Disciplina);
		if(Validade == true){
			request.setAttribute("Achou","true");
			request.getRequestDispatcher("MatricularAluno.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("MatricularAluno.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

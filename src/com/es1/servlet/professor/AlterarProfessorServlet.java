package com.es1.servlet.professor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Professor;
import com.es1.dao.BDProfessor;
import com.es1.service.ProfessorService;

/**
 * Servlet implementation class AlterarProfessorServlet
 */
@WebServlet("/AlterarProfessorServlet")
public class AlterarProfessorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlterarProfessorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfessorService professorservice = new ProfessorService();
		String Nome = request.getParameter("Nome");
		int Fone = Integer.parseInt(request.getParameter("Fone"));
		String Email = request.getParameter("Email");
		int CPF = Integer.parseInt(request.getParameter("CPF"));
		int RG = Integer.parseInt(request.getParameter("RG"));
		Boolean Achouu = professorservice.AlterarProfessor(Nome,Fone, Email, CPF, RG);
		if (Achouu == true) {
			request.setAttribute("Achouu","true");
			request.getRequestDispatcher("TelaAlterarProfessor.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

package com.es1.servlet.professor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Professor;
import com.es1.dao.BDProfessor;

/**
 * Servlet implementation class ConsultarProfessorServlet
 */
@WebServlet("/ConsultarProfessorServlet")
public class ConsultarProfessorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarProfessorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Professor professor = new Professor();
		BDProfessor bdprofessor = new BDProfessor();
		String Nome = request.getParameter("Nome");
		Boolean Achou = bdprofessor.Search(professor, Nome);
		int Fone = professor.getTelefone();
		String Email = professor.getEmail();
		int CPF = professor.getCPF();
		int RG = professor.getRG();
		if (Achou == true) {
			request.setAttribute("Nome",Nome);
			request.setAttribute("Fone",Fone);
			request.setAttribute("Email",Email);
			request.setAttribute("CPF",CPF);
			request.setAttribute("RG",RG);
			request.getRequestDispatcher("TelaConsultarProfessor.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("ConsultaProfessor.jsp").forward(request,response);
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

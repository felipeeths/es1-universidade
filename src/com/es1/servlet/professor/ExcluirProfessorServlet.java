package com.es1.servlet.professor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Professor;
import com.es1.dao.BDProfessor;
import com.es1.service.ProfessorService;

/**
 * Servlet implementation class ExcluirProfessorServlet
 */
@WebServlet("/ExcluirProfessorServlet")
public class ExcluirProfessorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExcluirProfessorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfessorService professorservice = new ProfessorService();
		String Nome = request.getParameter("NomeExcluir");
		Boolean Achou = professorservice.ExcluirProfessor(Nome);
		if (Achou == true) {	
			request.setAttribute("Achou","true");
			request.getRequestDispatcher("ExcluirProfessor.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("ExcluirProfessor.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

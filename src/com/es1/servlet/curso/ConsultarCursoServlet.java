package com.es1.servlet.curso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Curso;
import com.es1.dao.BDCurso;
import com.es1.service.CursoService;

/**
 * Servlet implementation class ConsultarCursoServlet
 */
@WebServlet("/ConsultarCursoServlet")
public class ConsultarCursoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarCursoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CursoService cursoservice = new CursoService();
		String Nome = request.getParameter("Nome");
		Boolean Achou = cursoservice.SearchCurso(Nome);
		if (Achou == true) {
			request.setAttribute("Nome",Nome);
			request.getRequestDispatcher("TelaConsultarCurso.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("ConsultarCurso.jsp").forward(request,response);
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

package com.es1.servlet.curso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Curso;
import com.es1.dao.BDCurso;
import com.es1.service.AlunoService;
import com.es1.service.CursoService;

/**
 * Servlet implementation class ExcluirCursoServlet
 */
@WebServlet("/ExcluirCursoServlet")
public class ExcluirCursoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExcluirCursoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CursoService cursoservice = new CursoService();
		String NomeExcluir = request.getParameter("NomeExcluir");
		Boolean Achouu = cursoservice.ExcluirCurso(NomeExcluir);
		if (Achouu == true) {	
			request.setAttribute("Achou","true");
			request.getRequestDispatcher("ExcluirCurso.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("ExcluirCurso.jsp").forward(request,response);
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

package com.es1.servlet.curso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Curso;
import com.es1.dao.BDCurso;

/**
 * Servlet implementation class SearchCursoServlet
 */
@WebServlet("/SearchCursoServlet")
public class SearchCursoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchCursoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		String Nome = request.getParameter("NomeAlterar");
		Boolean Achou = bdcurso.Search(curso, Nome);
		int idCurso = curso.getIdCurso();
		if (Achou == true) {
			request.setAttribute("Nome",Nome);
			request.setAttribute("idCurso",idCurso);
			request.getRequestDispatcher("TelaAlterarCurso.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("AlterarCurso.jsp").forward(request,response);
		}	
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

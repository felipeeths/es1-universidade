package com.es1.servlet.curso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Curso;
import com.es1.dao.BDCurso;

/**
 * Servlet implementation class AlterarCursoServlet
 */
@WebServlet("/AlterarCursoServlet")
public class AlterarCursoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlterarCursoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		int IdCurso = curso.getIdCurso();
		String Nome = request.getParameter("NomeAlterar");
		Boolean Achouu = bdcurso.Search(curso, Nome);
		curso.setIdCurso(IdCurso);
		if (Achouu == false) {
			curso.setNomeCurso(Nome);
			bdcurso.Update(curso,IdCurso);
			request.setAttribute("Achouu","true");
			request.getRequestDispatcher("TelaAlterarCurso.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

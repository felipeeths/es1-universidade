package com.es1.servlet.aluno;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.service.AlunoService;

/**
 * Servlet implementation class CadastrarAlunoServlet
 */
@WebServlet("/CadastrarAlunoServlet")
public class CadastrarAlunoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CadastrarAlunoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		AlunoService alunoservice = new AlunoService();
		String Nome = request.getParameter("Nome");
		String Endereco = request.getParameter("Endereco");
		int Fone = Integer.parseInt(request.getParameter("Fone"));
		String Email = request.getParameter("Email");
		int CPF = Integer.parseInt(request.getParameter("CPF"));
		int RG = Integer.parseInt(request.getParameter("RG"));
		Boolean Validade = alunoservice.CadastrarAluno(Nome,Endereco,Fone,Email,CPF,RG);
		if(Validade == true){
			request.setAttribute("Achou","true");
			request.getRequestDispatcher("CadastroAluno.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("CadastroAluno.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

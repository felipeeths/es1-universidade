package com.es1.servlet.aluno;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.es1.service.AlunoService;

/**
 * Servlet implementation class AlterarAlunoServlet
 */
@WebServlet("/AlterarAlunoServlet")
public class AlterarAlunoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AlterarAlunoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		AlunoService alunoservice = new AlunoService();
		String Nome = request.getParameter("Nome");
		String Endereco = request.getParameter("Endereco");
		int Fone = Integer.parseInt(request.getParameter("Fone"));
		String Email = request.getParameter("Email");
		int CPF = Integer.parseInt(request.getParameter("CPF"));
		int RG = Integer.parseInt(request.getParameter("RG"));
		Boolean Achouu = alunoservice.AlterarAluno(Nome, Endereco, Fone, Email, CPF, RG);
		if (Achouu == true) {
			request.setAttribute("Achouu","true");
			request.getRequestDispatcher("TelaAlterarAluno.jsp").forward(request,response);
		}else{
			request.setAttribute("Achouu","false");
			request.getRequestDispatcher("TelaAlterarAluno.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

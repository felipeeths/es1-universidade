package com.es1.servlet.aluno;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.es1.classe.Aluno;
import com.es1.dao.BDAluno;

/**
 * Servlet implementation class ConsultarAlunoServlet
 */
@WebServlet("/ConsultarAlunoServlet")
public class ConsultarAlunoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarAlunoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		String Nome = request.getParameter("Nome");
		Boolean Achou = bdaluno.Search(aluno, Nome);
		int Fone = aluno.getTelefone();
		String Email = aluno.getEmail();
		String Endereco = aluno.getEndereco();
		int CPF = aluno.getCPF();
		int RG = aluno.getRG();
		if (Achou == true) {
			request.setAttribute("Nome",Nome);
			request.setAttribute("Fone",Fone);
			request.setAttribute("Email",Email);
			request.setAttribute("Endereco",Endereco);
			request.setAttribute("CPF",CPF);
			request.setAttribute("RG",RG);
			request.getRequestDispatcher("TelaConsultarAluno.jsp").forward(request,response);
		}else{
			request.setAttribute("Achou","false");
			request.getRequestDispatcher("ConsultaAluno.jsp").forward(request,response);
		}	
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

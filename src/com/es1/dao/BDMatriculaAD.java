package com.es1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.SQLException;

import com.es1.classe.MatriculaADisciplina;

public class BDMatriculaAD {
	Connection connection;
	int id;

	public BDMatriculaAD() {
		new ConnectionFactory();
		this.connection = ConnectionFactory.getConnection();
	}

	public void Insert(MatriculaADisciplina matriculad) {
		String sql = "INSERT INTO matricularalunod(idAluno,idDisciplina) VALUES(?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, matriculad.getIdAluno());
			stmt.setInt(2, matriculad.getIdDisciplina());
			stmt.execute();
			stmt.close();

		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}
	
}

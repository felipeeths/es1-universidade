package com.es1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import com.es1.classe.Aluno;

public class BDAluno {
	Connection connection;
	int id;

	public BDAluno() {
		new ConnectionFactory();
		this.connection = ConnectionFactory.getConnection();
	}

	public void Insert(Aluno aluno) {
		String sql = "INSERT INTO aluno(NomeAluno,Endereco,TelefoneAluno,EmailAluno,CPF,RG) VALUES(?,?,?,?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, aluno.getNomeAluno());
			stmt.setString(2, aluno.getEndereco());
			stmt.setInt(3, aluno.getTelefone());
			stmt.setString(4, aluno.getEmail());
			stmt.setInt(5, aluno.getCPF());
			stmt.setInt(6, aluno.getRG());
			stmt.execute();
			stmt.close();

		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Update(Aluno aluno) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "UPDATE aluno SET " + "NomeAluno='"
					+ aluno.getNomeAluno() + "', Endereco='"
					+ aluno.getEndereco() + "', TelefoneAluno='"
					+ aluno.getTelefone() + "', EmailAluno='"
					+ aluno.getEmail() + "', CPF='" + aluno.getCPF()
					+ "', RG='" + aluno.getRG() + "' WHERE idAluno="
					+ aluno.getIdAluno();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Delete(Aluno aluno) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "DELETE FROM aluno " + "WHERE NomeAluno = '"
					+ aluno.getNomeAluno() + "'";
			stmt.execute(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public boolean Search(Aluno aluno, String NomeConsulta) {
		boolean result = false;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM aluno " + "WHERE NomeAluno = '"
					+ NomeConsulta + "'";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("NomeAluno");
				if (NomeConsulta == null ? nome == null : NomeConsulta.equals(nome)) {
					result = true;
					aluno.setIdAluno(Integer.parseInt(rs.getString("idAluno")));
					aluno.setNomeAluno(rs.getString("NomeAluno"));
					aluno.setEndereco(rs.getString("Endereco"));
					aluno.setTelefone(Integer.parseInt(rs.getString("TelefoneAluno")));
					aluno.setEmail(rs.getString("EmailAluno"));
					aluno.setCPF(Integer.parseInt(rs.getString("CPF")));
					aluno.setRG(Integer.parseInt(rs.getString("RG")));
				}
			}
			statement.close();
		} catch (SQLException sqlex) {
			JOptionPane.showMessageDialog(null, sqlex.toString() + "" + 1,
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
		return result;
	}
}

package com.es1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://localhost/datab",
					"root", "Felipe290794");
		} catch (SQLException excecao) {
			Logger.getLogger(ConnectionFactory.class.getName()).log(
					Level.SEVERE, null, excecao);
			throw new RuntimeException("Erro no Server", excecao);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			Logger.getLogger(ConnectionFactory.class.getName()).log(
					Level.SEVERE, null, e);
			throw new RuntimeException("Erro no class", e);
		}
	}
}
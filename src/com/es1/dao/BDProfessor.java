package com.es1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import com.es1.classe.Professor;

public class BDProfessor {
	Connection connection;
	int id;

	public BDProfessor() {
		new ConnectionFactory();
		this.connection = ConnectionFactory.getConnection();
	}

	public void Insert(Professor professor) {
		String sql = "INSERT INTO professor (NomeProfessor,TelefoneProfessor,EmailProfessor,CPF_Professor,RG_Professor) VALUES(?,?,?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, professor.getNome());
			stmt.setInt(2, professor.getTelefone());
			stmt.setString(3, professor.getEmail());
			stmt.setInt(4, professor.getCPF());
			stmt.setInt(5, professor.getRG());
			stmt.execute();
			stmt.close();

		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Update(Professor professor) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "UPDATE professor SET " + "NomeProfessor='"
					+ professor.getNome() + "', TelefoneProfessor='"
					+ professor.getTelefone() + "', EmailProfessor='"
					+ professor.getEmail() + "', CPF_Professor='"
					+ professor.getCPF() + "', RG_Professor='"
					+ professor.getRG() + "' WHERE idProfessor="
					+ professor.getIdProfessor();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Delete(Professor professor) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "DELETE FROM professor " + "WHERE NomeProfessor = '"
					+ professor.getNome() + "'";
			stmt.execute(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public boolean Search(Professor professor, String NomeConsulta) {
		boolean result = false;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM professor " + "WHERE NomeProfessor = '"
					+ NomeConsulta + "'";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("NomeProfessor");
				if (NomeConsulta == null ? nome == null : NomeConsulta.equals(nome)) {
					result = true;
					professor.setIdProfessor(Integer.parseInt(rs.getString("idProfessor")));
					professor.setNome(rs.getString("NomeProfessor"));
					professor.setTelefone(Integer.parseInt(rs.getString("TelefoneProfessor")));
					professor.setEmail(rs.getString("EmailProfessor"));
					professor.setCPF(Integer.parseInt(rs.getString("CPF_Professor")));
					professor.setRG(Integer.parseInt(rs.getString("RG_Professor")));
				}
			}
			statement.close();
		} catch (SQLException sqlex) {
			JOptionPane.showMessageDialog(null, sqlex.toString() + "" + 1,
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
		return result;
	}

	
}

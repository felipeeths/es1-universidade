package com.es1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import com.es1.classe.Disciplina;

public class BDDisciplinas {
	Connection connection;
	int id;

	public BDDisciplinas() {
		new ConnectionFactory();
		this.connection = ConnectionFactory.getConnection();
	}

	public void Insert(Disciplina disciplina) {
		String sql = "INSERT INTO disciplina(NomeDisciplina,Optativa,Semestre,AnoLetivo,curso_idCurso) VALUES(?,?,?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, disciplina.getNomeDisciplina());
			stmt.setString(2, disciplina.getOptativa());
			stmt.setInt(3, disciplina.getSemestre());
			stmt.setInt(4, disciplina.getAnoLetivo());
			stmt.setInt(5, disciplina.getIdCurso());
			stmt.execute();
			stmt.close();

		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Update(Disciplina disciplina) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "UPDATE disciplina SET " + "NomeDisciplina='"
					+ disciplina.getNomeDisciplina() + "', Optativa='"
					+ disciplina.getOptativa() + "', Semestre='"
					+ disciplina.getSemestre() + "', AnoLetivo='"
					+ disciplina.getAnoLetivo() + "', curso_idCurso='"
					+ disciplina.getIdCurso() + "' WHERE idDisciplina="
							+ disciplina.getIdDisciplina();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Delete(Disciplina disciplina) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "DELETE FROM disciplina " + "WHERE NomeDisciplina = '"
					+ disciplina.getNomeDisciplina() + "'";
			stmt.execute(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public boolean Search(Disciplina disciplina, String NomeConsulta) {
		boolean result = false;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM disciplina " + "WHERE NomeDisciplina = '"
					+ NomeConsulta + "'";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("NomeDisciplina");
				if (NomeConsulta == null ? nome == null : NomeConsulta.equals(nome)) {
					result = true;
					disciplina.setIdDisciplina(Integer.parseInt(rs.getString("idDisciplina")));
					disciplina.setIdCurso(Integer.parseInt(rs.getString("curso_idCurso")));
					disciplina.setNomeDisciplina(rs.getString("NomeDisciplina"));
					disciplina.setSemestre(Integer.parseInt(rs.getString("Semestre")));
					disciplina.setAnoLetivo(Integer.parseInt(rs.getString("AnoLetivo")));
					disciplina.setOptativa(rs.getString("Optativa"));
				}
			}
			statement.close();
		} catch (SQLException sqlex) {
			JOptionPane.showMessageDialog(null, sqlex.toString() + "" + 1,
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
		return result;
	}
	public boolean Search2(Disciplina disciplina, String NomeConsulta) {
		boolean result = false;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM disciplina " + "WHERE NomeDisciplina = '"
					+ NomeConsulta + "'";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("NomeDisciplina");
				if (NomeConsulta == null ? nome == null : NomeConsulta.equals(nome)) {
					result = true;
					disciplina.setIdDisciplina(Integer.parseInt(rs.getString("idDisciplina")));
					disciplina.setNomeDisciplina(rs.getString("NomeDisciplina"));
					disciplina.setOptativa(rs.getString("Optativa"));
					disciplina.setIdCurso(Integer.parseInt(rs.getString("curso_idCurso")));
				}
			}
			statement.close();
		} catch (SQLException sqlex) {
			JOptionPane.showMessageDialog(null, sqlex.toString() + "" + 1,
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
		return result;
	}
}

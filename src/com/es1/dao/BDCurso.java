package com.es1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import com.es1.classe.Curso;


public class BDCurso {
	Connection connection;
	int id;

	public BDCurso() {
		new ConnectionFactory();
		this.connection = ConnectionFactory.getConnection();
	}

	public void Insert(Curso curso) {
		String sql = "INSERT INTO curso(NomeCurso) VALUES(?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, curso.getNomeCurso());
			stmt.execute();
			stmt.close();

		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Update(Curso curso,int Id) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "UPDATE curso SET " + "NomeCurso = '"
					+ curso.getNomeCurso() + "' WHERE idCurso = '" 
					+ Id  + "'";
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Delete(Curso curso) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "DELETE FROM curso " + "WHERE NomeCurso = '"
					+ curso.getNomeCurso() + "'";
			stmt.execute(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public boolean Search(Curso curso, String NomeConsulta) {
		boolean result = false;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM curso " + "WHERE NomeCurso = '"
					+ NomeConsulta + "'";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("NomeCurso");
				if (NomeConsulta == null ? nome == null : NomeConsulta.equals(nome)) {
					result = true;
					curso.setIdCurso(Integer.parseInt(rs.getString("idCurso")));
				}
			}
			statement.close();
		} catch (SQLException sqlex) {
			JOptionPane.showMessageDialog(null, sqlex.toString() + "" + 1,
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
		return result;
	}
	public boolean Search2(Curso curso,String NomeConsulta) {
		boolean result = false;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM curso " + "WHERE idCurso = '"
					+ NomeConsulta + "'";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("idCurso");
				if (NomeConsulta == null ? nome == null : NomeConsulta.equals(nome)) {
					result = true;
					curso.setIdCurso(Integer.parseInt(rs.getString("idCurso")));
					curso.setNomeCurso(rs.getString("NomeCurso"));
				}
			}
			statement.close();
		} catch (SQLException sqlex) {
			JOptionPane.showMessageDialog(null, sqlex.toString() + "" + 1,
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
		return result;
	}
	
	
}

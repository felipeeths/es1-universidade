package com.es1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import com.es1.classe.Matricula;
import com.es1.classe.MatricularProfessor;

public class BDMatricularProf {
	Connection connection;
	int id;

	public BDMatricularProf() {
		new ConnectionFactory();
		this.connection = ConnectionFactory.getConnection();
	}

	public void Insert(MatricularProfessor matricularprofessor) {
		String sql = "INSERT INTO disciplinaprofessor(disciplina_idDisciplina,professor_idprofessor,dataInicio) VALUES(?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, matricularprofessor.getIdDisciplina());
			stmt.setInt(2, matricularprofessor.getIdProfessor());
			stmt.setString(3, matricularprofessor.getDataInicio());
			stmt.execute();
			stmt.close();

		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}
	
	
	public void Update(Matricula matricula) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "UPDATE matriculaaluno SET " + "dataMatricula='"
					+ matricula.getData() + "', aluno_idAluno='"
					+ matricula.getIdAluno() + "', curso_idCurso='"
					+ matricula.getIdCurso() + "' WHERE idMatricula="
							+ matricula.getIdMatricula();
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public void Delete(Matricula matricula) {
		try {
			Statement stmt = connection.createStatement();
			String sql = "DELETE FROM matriculaaluno " + "WHERE idMatricula = '"
					+ matricula.getIdMatricula() + "'";
			stmt.execute(sql);
			stmt.close();
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public boolean Search(Matricula matricula, String NomeConsulta) {
		boolean result = false;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM matriculaaluno " + "WHERE aluno_idAluno = '"
					+ NomeConsulta + "'";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("aluno_idAluno");
				if (NomeConsulta == null ? nome == null : NomeConsulta.equals(nome)) {
					result = true;
					matricula.setIdMatricula(Integer.parseInt(rs.getString("idMatriculaAluno")));
					matricula.setIdCurso(Integer.parseInt(rs.getString("curso_idCurso")));
					matricula.setData(rs.getString("dataMatricula"));
					matricula.setIdAluno(Integer.parseInt(rs.getString("aluno_idAluno")));
				}
			}
			statement.close();
		} catch (SQLException sqlex) {
			JOptionPane.showMessageDialog(null, sqlex.toString() + "" + 1,
					"Aviso", JOptionPane.WARNING_MESSAGE);
		}
		return result;
	}
}

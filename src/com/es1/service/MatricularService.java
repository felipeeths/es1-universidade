package com.es1.service;

import java.util.Calendar;

import com.es1.classe.Aluno;
import com.es1.classe.Curso;
import com.es1.classe.Disciplina;
import com.es1.classe.Matricula;
import com.es1.classe.MatriculaADisciplina;
import com.es1.classe.MatricularProfessor;
import com.es1.classe.Professor;
import com.es1.dao.BDAluno;
import com.es1.dao.BDCurso;
import com.es1.dao.BDDisciplinas;
import com.es1.dao.BDMatricula;
import com.es1.dao.BDMatriculaAD;
import com.es1.dao.BDMatricularProf;
import com.es1.dao.BDProfessor;

public class MatricularService {

	public boolean MatricularAluno(String Nome, String Curso){
		boolean Sucesso = false;
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		Calendar cl = Calendar.getInstance();
		Matricula matricula = new Matricula();
		BDMatricula bdmatricula = new BDMatricula();
		Boolean AchouAluno = bdaluno.Search(aluno, Nome);
		Boolean AchouCurso = bdcurso.Search(curso, Curso);
		if(AchouAluno == true && AchouCurso == true){
			matricula.setIdAluno(aluno.getIdAluno());
			matricula.setIdCurso(curso.getIdCurso());
			String idM = String.valueOf(matricula.getIdAluno());
			Boolean AchouMatricula = bdmatricula.Search(matricula,idM);
			if(AchouMatricula == false){
				String data = String.valueOf(cl.get(Calendar.YEAR)) +"-"+ String.valueOf(cl.get(Calendar.MONTH)+1) +"-"+ String.valueOf(cl.get(Calendar.DATE));
				matricula.setData(data);
				bdmatricula.Insert(matricula);
				Sucesso = true;
			}
		}
		return Sucesso;
	}
	public boolean MatricularADisciplina(String Nome,String Disciplina){
		boolean Sucesso = false;
		MatriculaADisciplina matriculad = new MatriculaADisciplina();
		BDMatriculaAD bdmatriculad = new BDMatriculaAD();
		Disciplina disciplina = new Disciplina();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		boolean AchouDisciplina = bddisciplina.Search(disciplina,Disciplina);
		boolean AchouAluno = bdaluno.Search(aluno, Nome);
		if(AchouDisciplina == true && AchouAluno){
			matriculad.setIdAluno(aluno.getIdAluno());
			matriculad.setIdDisciplina(disciplina.getIdDisciplina());
			bdmatriculad.Insert(matriculad);
			Sucesso = true;
		}
		return Sucesso;
	}
	public boolean MatricularProfessor(String NomeProfessor,String Disciplina){
		boolean Sucesso = false;
		Professor professor = new Professor();
		BDProfessor bdprofessor = new BDProfessor();
		Disciplina disciplina = new Disciplina();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		MatricularProfessor matricularprofessor = new MatricularProfessor();
		BDMatricularProf bdmatricularprof = new BDMatricularProf();
		boolean AchouProfessor = bdprofessor.Search(professor, NomeProfessor);
		boolean AchouDisciplina = bddisciplina.Search(disciplina, Disciplina);
		if(AchouProfessor == true && AchouDisciplina == true){
			int idProfessor = professor.getIdProfessor();
			int idDisciplina = disciplina.getIdDisciplina();
			matricularprofessor.setIdProfessor(idProfessor);
			matricularprofessor.setIdDisciplina(idDisciplina);
			bdmatricularprof.Insert(matricularprofessor);
			Sucesso = true;
		}
		return Sucesso;
	}
}

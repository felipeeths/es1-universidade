package com.es1.service;

import com.es1.classe.Aluno;
import com.es1.dao.BDAluno;

public class AlunoService {

	public boolean CadastrarAluno(String Nome, String Endereco, int Telefone,
			String Email, int CPF, int RG) {
		boolean cadastro = false;
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Boolean Achou = bdaluno.Search(aluno, Nome);
		if (Achou == false) {
			aluno.setNomeAluno(Nome);
			aluno.setEndereco(Endereco);
			aluno.setTelefone(Telefone);
			aluno.setEmail(Email);
			aluno.setCPF(CPF);
			aluno.setRG(RG);
			bdaluno.Insert(aluno);
			cadastro = true;
		}
		return cadastro;
	}

	public boolean AlterarAluno(String Nome, String Endereco, int Telefone,
			String Email, int CPF, int RG) {
		boolean alterar = false;
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Boolean Achou = bdaluno.Search(aluno, Nome);
		if (Achou == true) {
			aluno.setNomeAluno(Nome);
			aluno.setEndereco(Endereco);
			aluno.setTelefone(Telefone);
			aluno.setEmail(Email);
			aluno.setCPF(CPF);
			aluno.setRG(RG);
			bdaluno.Update(aluno);
			alterar = true;
		}
		return alterar;
	}

	public boolean ExcluirAluno(String Nome) {
		boolean excluir = false;
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Boolean Achou = bdaluno.Search(aluno, Nome);
		if (Achou == true) {
			aluno.setNomeAluno(Nome);
			bdaluno.Delete(aluno);
			excluir = true;
		}
		return excluir;
	}

	public boolean ConsultarAluno(String Nome) {
		boolean consultar = false;
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Boolean Achou = bdaluno.Search(aluno, Nome);
		if (Achou == true) {
			consultar = true;
		}
		return consultar;

	}

	public boolean SearchAluno(String Nome) {
		boolean search = false;
		Aluno aluno = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Boolean Achou = bdaluno.Search(aluno, Nome);
		if (Achou == true) {
			search = true;
		}
		return search;
	}
}

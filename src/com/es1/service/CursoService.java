package com.es1.service;

import com.es1.classe.Aluno;
import com.es1.classe.Curso;
import com.es1.classe.Disciplina;
import com.es1.dao.BDAluno;
import com.es1.dao.BDCurso;
import com.es1.dao.BDDisciplinas;

public class CursoService {
	
	public boolean CadastrarCurso(String Nome) {
		boolean cadastro = false;
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		Boolean Achou = bdcurso.Search(curso, Nome);
		if (Achou == false) {
			curso.setNomeCurso(Nome);
			bdcurso.Insert(curso);
			cadastro = true;
		}
		return cadastro;
	}

	public boolean AlterarDisciplina(String Nome, String Optativa, String Curso) {
		boolean alterar = false;
		Disciplina disciplina = new Disciplina();
		Curso curso = new Curso();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		BDCurso bdcurso = new BDCurso();
		Boolean Achou = bddisciplina.Search(disciplina, Nome);
		Boolean AcharCurso = bdcurso.Search(curso, Curso);
		if (Achou == true && AcharCurso == true) {
			disciplina.setNomeDisciplina(Nome);
			disciplina.setOptativa(Optativa);
			disciplina.setIdCurso(curso.getIdCurso());
			bddisciplina.Update(disciplina);
			alterar = true;
		}
		return alterar;
	}

	public boolean ExcluirCurso(String Nome) {
		boolean excluir = false;
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		Boolean Achou = bdcurso.Search(curso, Nome);
		if (Achou == true) {
			curso.setNomeCurso(Nome);
			bdcurso.Delete(curso);
			excluir = true;
		}
		return excluir;
	}

	public boolean ConsultarProfessor(String Nome) {
		boolean consultar = false;
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		Boolean Achou = bdcurso.Search(curso, Nome);
		if (Achou == true) {
			consultar = true;
		}
		return consultar;

	}

	public boolean SearchCurso(String Nome) {
		boolean search = false;
		Curso curso = new Curso();
		BDCurso bdcurso = new BDCurso();
		Boolean Achou = bdcurso.Search(curso, Nome);
		if (Achou == true) {
			search = true;
		}
		return search;
	}
}

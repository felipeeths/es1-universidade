package com.es1.service;

import com.es1.classe.Aluno;
import com.es1.classe.Curso;
import com.es1.classe.Disciplina;
import com.es1.dao.BDAluno;
import com.es1.dao.BDCurso;
import com.es1.dao.BDDisciplinas;

public class DisciplinaService {

	public boolean CadastrarDisciplina(String Nome, String Optativa,int Semestre, int AnoLetivo, String Curso) {
		boolean cadastro = false;
		Disciplina disciplina = new Disciplina();
		Curso curso = new Curso();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		BDCurso bdcurso = new BDCurso();
		Boolean Achou = bddisciplina.Search(disciplina, Nome);
		Boolean AcharCurso = bdcurso.Search(curso, Curso);
		if (Achou == false && AcharCurso == true) {
			disciplina.setNomeDisciplina(Nome);
			disciplina.setOptativa(Optativa);
			disciplina.setSemestre(Semestre);
			disciplina.setAnoLetivo(AnoLetivo);
			disciplina.setIdCurso(curso.getIdCurso());
			bddisciplina.Insert(disciplina);
			cadastro = true;
		}
		return cadastro;
	}

	public boolean AlterarDisciplina(String Nome, String Optativa, int Semestre,int AnoLetivo, String Curso) {
		boolean alterar = false;
		Disciplina disciplina = new Disciplina();
		Curso curso = new Curso();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		BDCurso bdcurso = new BDCurso();
		Boolean Achou = bddisciplina.Search(disciplina, Nome);
		Boolean AcharCurso = bdcurso.Search(curso, Curso);
		if (Achou == true && AcharCurso == true) {
			disciplina.setNomeDisciplina(Nome);
			disciplina.setOptativa(Optativa);
			disciplina.setSemestre(Semestre);
			disciplina.setAnoLetivo(AnoLetivo);
			disciplina.setIdCurso(curso.getIdCurso());
			bddisciplina.Update(disciplina);
			alterar = true;
		}
		return alterar;
	}

	public boolean ExcluirDisciplina(String Nome) {
		boolean excluir = false;
		Disciplina disciplina = new Disciplina();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		Boolean Achou = bddisciplina.Search(disciplina, Nome);
		if (Achou == true) {
			disciplina.setNomeDisciplina(Nome);
			bddisciplina.Delete(disciplina);
			excluir = true;
		}
		return excluir;
	}

	public boolean ConsultarProfessor(String Nome) {
		boolean consultar = false;
		Aluno professor = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Boolean Achou = bdaluno.Search(professor, Nome);
		if (Achou == true) {
			consultar = true;
		}
		return consultar;

	}

	public boolean SearchDisciplina(String Nome) {
		boolean search = false;
		Disciplina disciplina = new Disciplina();
		BDDisciplinas bddisciplina = new BDDisciplinas();
		Boolean Achou = bddisciplina.Search(disciplina, Nome);
		if (Achou == true) {
			search = true;
		}
		return search;
	}

}

package com.es1.service;

import com.es1.classe.Aluno;
import com.es1.classe.Professor;
import com.es1.dao.BDAluno;
import com.es1.dao.BDProfessor;

public class ProfessorService {
	public boolean CadastrarProfessor(String Nome, int Telefone, String Email, int CPF, int RG){
		boolean cadastro = false;
		Professor professor = new Professor();
		BDProfessor bdprofessor = new BDProfessor();
		Boolean Achou = bdprofessor.Search(professor, Nome);
		if(Achou == false){
			professor.setNome(Nome);
			professor.setTelefone(Telefone);
			professor.setEmail(Email);
			professor.setCPF(CPF);
			professor.setRG(RG);
			bdprofessor.Insert(professor);
			cadastro = true;
		}
		return cadastro;
	}
	
	public boolean AlterarProfessor(String Nome, int Telefone, String Email, int CPF, int RG){
		boolean alterar = false;
		Professor professor = new Professor();
		BDProfessor bdprofessor = new BDProfessor();
		Boolean Achou = bdprofessor.Search(professor, Nome);
		if(Achou == true){
			professor.setNome(Nome);
			professor.setTelefone(Telefone);
			professor.setEmail(Email);
			professor.setCPF(CPF);
			professor.setRG(RG);
			bdprofessor.Update(professor);
			alterar = true;
		}
		return alterar;
		}
	public boolean ExcluirProfessor(String Nome){
		boolean excluir = false;
		Professor professor = new Professor();
		BDProfessor bdprofessor = new BDProfessor();
		Boolean Achou = bdprofessor.Search(professor, Nome);
		if(Achou == true){
			professor.setNome(Nome);
			bdprofessor.Delete(professor);
			excluir = true;
		}
		return excluir;
	}
	public boolean ConsultarProfessor(String Nome){
		boolean consultar = false;
		Aluno professor = new Aluno();
		BDAluno bdaluno = new BDAluno();
		Boolean Achou = bdaluno.Search(professor, Nome);
		if(Achou == true){
			consultar = true;
		}
		return consultar;
		
	}
	public boolean SearchProfessor(String Nome){
		boolean search = false;
		Professor professor = new Professor();
		BDProfessor bdprofessor = new BDProfessor();
		Boolean Achou = bdprofessor.Search(professor, Nome);
		if(Achou == true){
			search = true;
		}
		return search;
	}
}

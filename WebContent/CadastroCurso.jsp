<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html lang="en">
  <head>

    <title>ES1 - Cadastro Curso</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
     <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.jsp">Inicio</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="MenuCurso.jsp">Voltar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
	  <br>
        <h3>Cadastrar Curso</h3>
      </div>
    </div>

    <div class="container">
		<%
			try {
				Object Resposta = request.getAttribute("Achou");
				if (Resposta.equals("true")) {
					out.println("<div class='panel panel-success'>"
							+ "<div class='panel-heading'>"
							+ "  <h3 class='panel-title'>Sucesso</h3>"
							+ "</div>" + "<div class='panel-body'>"
							+ "  Cadastro efetuado com sucesso" + "</div>"
							+ "</div>");
				} else {
					out.println("<div class='panel panel-success'>"
							+ "<div class='panel-heading'>"
							+ "  <h3 class='panel-title'>Erro</h3>" + "</div>"
							+ "<div class='panel-body'>"
							+ "  Curso ja cadastrado com esse nome"
							+ "</div>" + "</div>");
				}
			} catch (NullPointerException ex) {
			}
		%>

		<form class="form-signin" action="CadastrarCServlet">
			<h2 class="form-signin-heading"></h2>
			<label for="nome" class="sr-only">Nome</label> <input type="text"
				name="Nome" id="nome" class="form-control" placeholder="Nome"
				required autofocus> <br>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Cadastrar</button>
		</form>


		<hr>

      <footer>
        <p>&copy; Felipe,Flavio,Gustavo 2015</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
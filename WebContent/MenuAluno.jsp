<!DOCTYPE html>
<html lang="en">
  <head>
    
    <title>ES1 - Menu Aluno</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

  </head>
  
  <body>
     <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.jsp">Inicio</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.jsp">Voltar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h2>Aluno</h2>
        <p>Gerenciar Alunos</p>
      </div>
    </div>
 <center>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-2">
          <h3>Cadastrar Aluno</h3>
                   <p><a class="btn btn-default" href="CadastroAluno.jsp" role="button">Ir para Cadastro &raquo;</a></p>
        </div>
        <div class="col-md-2">
          <h3>Alterar Aluno</h3>
                    <p><a class="btn btn-default" href="AlteraAluno.jsp" role="button">Ir para Alterar &raquo;</a></p>
       </div>
        <div class="col-md-2">
          <h3>Excluir Aluno</h3>
                  <p><a class="btn btn-default" href="ExcluiAluno.jsp" role="button">Ir para Excluir &raquo;</a></p>
        </div>
		 <div class="col-md-2">
          <h3>Consultar Aluno</h3>
                   <p><a class="btn btn-default" href="ConsultaAluno.jsp" role="button">Ir para Consultar &raquo;</a></p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Felipe,Flavio,Gustavo 2015</p>
      </footer>
    </div> <!-- /container --></center>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>